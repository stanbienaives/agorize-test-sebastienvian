class TeamsController < ApplicationController
  def index

    @teams = Team.eager_load(:users).eager_load(:assets)
                .sort_by{ |team| team.users.size }
                .reverse
                .map do |team|
                    { 
                      name: team.name,
                      assets: team.assets.group_by(&:kind).map { |kind,group|  { kind: kind, size: group.size } },
                      users:  team.users,
                    }
                end

    #render json: @teams

  end
end
