class AddIndexes < ActiveRecord::Migration
  def change
    add_index :users, :team_id, :name => 'user_team_id_ix'
    add_index :assets, :team_id, :name => 'asset_team_id_ix'
  end
end
